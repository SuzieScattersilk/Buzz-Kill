extends AnimationPlayer 
# just found out about animTrees.
# so, this file has been pared down to callback-related functions.

# A variable to hold the funcref that will be called from the animations
var callback_function = null

func _ready():
	connect("animation_finished", self, "animation_ended")
	
# keeping this here - it might need to be explicitly defined.
func animation_ended(_anim_name):
	pass

func animation_callback():
	if callback_function == null:
		print ("AnimationPlayer_Manager.gd -- WARNING: No callback function for the animation to call!")
	else:
		callback_function.call_func()
