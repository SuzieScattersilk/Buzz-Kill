extends Spatial

var audio_node
var globals
var should_loop


func _ready():
	# Get the audio player node, connect the finished signal, and assure it's not playing anything.
	audio_node = $"3D_Sound_Stream"
	audio_node.connect("finished", self, "sound_finished")
	audio_node.stop()
	
	# Get the globals script
	globals = get_node("/root/Globals")


func play_sound(sound_def_name):
	if !(sound_def_name in globals.audio_clips):
		print ("No audio defined in globals.audio_clips, cannot play sound")
		return
	var audio_stream = globals.audio_clips[sound_def_name]
	globals.created_audio.append(self)
	if audio_stream == null:
		print ("No audio stream passed, cannot play sound")
		globals.created_audio.remove(globals.created_audio.find(self))
		return
	audio_node.stream = audio_stream
	audio_node.play(0.0)
	

func sound_finished():
	if should_loop:
		# Start playing again, at the beginning
		audio_node.play(0.0)
	else:
		# Destroy/Free this sound.
		globals.created_audio.remove(globals.created_audio.find(self))
		audio_node.stop()
