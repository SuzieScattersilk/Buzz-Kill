extends Spatial

# The audio player node.
var audio_node = null
# A variable to track whether or not we should loop
var should_loop = false
# The globals autoload script
var globals = null

func _ready():
	# Get the audio player node, connect the finished signal, and assure it's not playing anything.
	audio_node = $Audio_Stream_Player
	audio_node.connect("finished", self, "sound_finished")
	audio_node.stop()
	
	# Get the globals script
	globals = get_node("/root/Globals")


func play_sound(audio_stream, position=null):
	if audio_stream == null:
		print ("No audio stream passed, cannot play sound")
		globals.created_audio.remove(globals.created_audio.find(self))
		queue_free()
		return
	audio_node.stream = audio_stream
	audio_node.play(0.0)
	

func sound_finished():
	if should_loop:
		# Start playing again, at the beginning
		audio_node.play(0.0)
	else:
		# Destroy/Free this sound.
		globals.created_audio.remove(globals.created_audio.find(self))
		audio_node.stop()
		queue_free()
