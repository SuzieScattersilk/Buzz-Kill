# all that code that was in Player.gd pertaining to joypads,
# now in its own class. Epic...

extends Node

const JOYPAD_DEADZONE = 0.15
var JOYPAD_SENSITIVITY = 2

var globals

func _ready():
	globals = get_node("/root/Globals")
	JOYPAD_SENSITIVITY = globals.joypad_sensitivity

func connected():
	return Input.get_connected_joypads().size() > 0
	
func get_left_stick():
	return Input.get_vector("movement_left", "movement_right", "movement_backward", "movement_forward")
	
func get_right_stick():
	return Input.get_vector("look_left", "look_right", "look_up", "look_down")
