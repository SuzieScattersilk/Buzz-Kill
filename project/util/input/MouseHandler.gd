# pretty much all of the mouse code, put into its own little node.

extends Node

var MOUSE_SENSITIVITY = 0.25
# The value of the scroll wheel (relative to our current weapon)
var mouse_scroll_value = 0
# How much a single scroll action increases mouse_scroll_value
const MOUSE_SENSITIVITY_SCROLL_WHEEL = 0.5

func _ready():
	pass

func mouse_available():
	return Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED
	
func mouse_moving(event):
	return event is InputEventMouseMotion and mouse_available()

func mouse_button_hit(event):
	return event is InputEventMouseButton and mouse_available()

func scroll_input(event):
	if mouse_button_hit(event):
		if event.button_index == BUTTON_WHEEL_UP or event.button_index == BUTTON_WHEEL_DOWN:
			if event.button_index == BUTTON_WHEEL_UP:
				mouse_scroll_value += MOUSE_SENSITIVITY_SCROLL_WHEEL
			elif event.button_index == BUTTON_WHEEL_DOWN:
				mouse_scroll_value -= MOUSE_SENSITIVITY_SCROLL_WHEEL
				
func clamped_scroll_input(event, min_value, max_value):
	scroll_input(event)
	mouse_scroll_value = clamp(mouse_scroll_value, min_value, max_value)
	
func get_mouse_scroll_value():
	return mouse_scroll_value 
	
func set_mouse_scroll_value(value):
	mouse_scroll_value = value

