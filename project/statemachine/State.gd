class_name State
extends Node

var state_machine = null
var thinker

func _ready() -> void:
	yield(owner, "ready")
	thinker = owner

# corresponds to the actor's `_process()` function
func process(_delta: float) -> void:
	pass

# corresponds to the actor's `_physics_process()` function
func physics_process(_delta: float) -> void:
	pass

# _msg is any state data. can be omitted.
func enter(_msg := {}) -> void:
	pass
	
func exit() -> void:
	pass
