# mad effin' props to Nathan Lovato for this design pattern.

class_name StateMachine
extends Node

signal transitioned_to(state_name)

export var initial_state := NodePath()
onready var state: State = get_node(initial_state)

func _ready() -> void:
	yield(owner, "ready")
	for child in get_children():
		child.state_machine = self
	state.enter()
	
func process(delta: float) -> void:
	state.process(delta)
	
func physics_process(delta: float) -> void:
	state.physics_process(delta)
	
func transition_to(next_state: String, msg: Dictionary = {}) -> void:
	if not has_node(next_state):
		print("WARNING: ", next_state, " not defined for ", owner)
		return
	state.exit()
	state = get_node(next_state)
	state.enter(msg)
	emit_signal("transitioned_to", state.name)
