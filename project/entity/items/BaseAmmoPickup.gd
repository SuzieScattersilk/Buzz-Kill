extends BasePickup

#todo: fix this tacky redundancy
export var amount = 0
export(String, "None", "Pistol") var for_weapon = ""

func _ready():
	pass
	
func pickup_effect(body):
	if body.has_method("add_ammo"):
		if !body.inventory.has_weapon(for_weapon):
#			print(body, " is adding ", for_weapon, " to its inventory." )
			var weapon = body.get_node("Rotation_Helper/Gun_Fire_Points/"+for_weapon+"_Point")
			body.inventory.put_weapon(weapon)
#			print(body, " has added ", for_weapon, " to its inventory." )
			play_pickup_sound(body)
		var wep = body.inventory.get(for_weapon)
		if wep != null:
			if !wep.is_full():
				body.add_ammo(for_weapon, amount)
				play_pickup_sound(body)
			handle_despawn()
