extends Spatial
class_name BasePickup

export var pickup_sound = ""
export var respawns = true #does this item respawn?
export var respawn_time = 0 #seconds. 0 = instantly respawns.

var sound_player

var respawn_timer = 0

func _ready():
	$Holder/Pickup_Trigger.connect("body_entered", self, "trigger_body_entered")
	sound_player = $Sound_Player
	respawn_time = respawn_time if respawns else 0

func _physics_process(delta):
	if respawn_timer <= 0 or respawn_time <= 0:
		enable()
	else:
		respawn_timer -= delta
		
func enable():
	$Holder/Pickup_Trigger/Shape_Kit.disabled = false
	$Holder/Kit.visible = true
		
func disable():
	$Holder/Pickup_Trigger/Shape_Kit.disabled = true
	$Holder/Kit.visible = false
	
func trigger_body_entered(body):
	pickup_effect(body)
		
func handle_despawn():
	if respawns:
		respawn_timer = respawn_time
		disable()
	else:
		queue_free()
		
func play_pickup_sound(_body):
	sound_player.play_sound(pickup_sound)
		
func pickup_effect(_body):
	pass #put logic for whatever the pickup is meant to do here in children
