extends BasePickup

export var amount = 0
export var allow_overheal: bool = false

func _ready():
	pass

func heal_player(body):
	if body.health < body.MAX_HEALTH:
		play_pickup_sound(body)
		body.add_health(amount)
		handle_despawn()
		
func overheal_player(body):
	if body.health < body.MAX_OVERHEAL:
		play_pickup_sound(body)
		body.add_overheal(amount)
		handle_despawn()

func pickup_effect(body):
	if allow_overheal:
		if body.has_method("add_overheal"):
			overheal_player(body)
	else:
		if body.has_method("add_health"):
			heal_player(body)
