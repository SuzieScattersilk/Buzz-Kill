extends Spatial
class_name BaseWeapon

export var weapon_name = "" # name
export var weapon_slot = 1 # number for the inventory ordering.
export(PackedScene) var ammo_type
export var consumes_ammo = true
export var max_ammo = 1000

#sound and anim tags.
const NULL_TAG = "Idle_unarmed"
const IDLE_TAG = "_idle"
const FIRE_TAG = "_fire"
const EQUIP_TAG = "_equip"
const UNEQUIP_TAG = "_unequip"

# generated from the weapon name - Just the name of the weapon formatted like "Weapon".
# at least for now.
var IDLE_NAME
var FIRE_NAME
var EQUIP_NAME
var UNEQUIP_NAME

var is_weapon_enabled = false
onready var projectile # derived from ammo_type
var ammo_in_weapon = 0
var player_node = null #is assigned in PlayerInventory.setup_weapon()

func _to_string():
	return str(weapon_name) + \
	"; Slot " + str(weapon_slot) + \
	"; uses " + str(ammo_type) + \
	"; uses ammo? " + str(consumes_ammo) + \
	"; max " + str(max_ammo)
	

func _ready():
	#self.projectile = load(ammo_type+"_Scene.tscn") #has to be loaded on the fly
	self.projectile = ammo_type
	self.IDLE_NAME = weapon_name+IDLE_TAG
	self.FIRE_NAME = weapon_name+FIRE_TAG
	self.EQUIP_NAME = weapon_name+EQUIP_TAG
	self.UNEQUIP_NAME = weapon_name+UNEQUIP_TAG
	self.max_ammo = max_ammo if consumes_ammo else 0

func play_sound(sound):
	player_node.sound_player.play_sound(sound)
	
func spawn_projectile():
	var clone = projectile.instance()
	clone.shooter_node = self.player_node # IMPORTANT - check if player is the caster
	var scene_root = get_tree().root.get_children()[0]
	scene_root.add_child(clone)
	clone.global_transform = self.global_transform
	if clone is PhysicsProjectile:
		clone.apply_central_impulse(clone.global_transform.basis.z * clone.muzzle_velocity)
	
func fire_weapon():
	player_node.animation_node.travel(FIRE_NAME)
	spawn_projectile()
	if consumes_ammo:
		consume_ammo(1) #safer than just subtracting - prevents amounts <0
	play_sound(FIRE_NAME)
	
func consume_ammo(amount):
	ammo_in_weapon -= amount
	ammo_in_weapon = clamp(ammo_in_weapon, 0, max_ammo)
	
func add_ammo(amount):
	ammo_in_weapon += amount
	ammo_in_weapon = clamp(ammo_in_weapon, 0, max_ammo)
	
func equip_weapon():
#	print("from: ", player_node.animation_node.get_current_node())
	player_node.animation_node.travel(EQUIP_NAME)
#	print("to ", player_node.animation_node.get_current_node())
	is_weapon_enabled = true
	play_sound(EQUIP_NAME)
	return true
	
func unequip_weapon():
#	print("from: ", player_node.animation_node.get_current_node())
	player_node.animation_node.travel(UNEQUIP_NAME)
#	print("to ", player_node.animation_node.get_current_node())
	is_weapon_enabled = false
	return true
		
func reset_weapon():
	ammo_in_weapon = 0
	is_weapon_enabled = false

func is_full():
	return ammo_in_weapon == max_ammo

func is_empty():
	return ammo_in_weapon == 0
