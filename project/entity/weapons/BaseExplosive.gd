extends BaseProjectile

var collision_shape
var mesh
var blast_area
var explosion_particles

func _ready():
	collision_shape = $Collision_Shape
	mesh = $MeshInstance
	blast_area = $Blast_Area
	explosion_particles = $Explosion
	
	# Make sure the explosion particles are not emitting, and make sure one_shot is enabled.
	explosion_particles.emitting = false
	explosion_particles.one_shot = true
#	print("Sound player: ", sound_player)

func apply_effect():
	explosion_particles.emitting = true
	mesh.visible = false
	collision_shape.disabled = true
	mode = RigidBody.MODE_STATIC
	var bodies = blast_area.get_overlapping_bodies()
	for body in bodies:
		target = body
		.apply_effect() #call to parent

