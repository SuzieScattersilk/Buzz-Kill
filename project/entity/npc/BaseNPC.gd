class_name BaseNPC
extends KinematicBody

export var move_speed = 50
export var max_health = 4
export var group = "npc"
export var turn_speed = 5

var player = null
var dead = false
var direction
var health
var sound_player
var state_machine
var eyes

func _ready():
	add_to_group(group)
	sound_player = $Sound_Player
	state_machine = $StateMachine
	eyes = $Eyes
	direction = Vector3.ZERO
	health = max_health
	
func _physics_process(delta):
	if dead:
		queue_free()
	else:
		state_machine.physics_process(delta)
	
func die():
	sound_player.play_sound("Flesh_Burst")
	$CollisionShape.disabled = true
	dead = true
	
func wander():
	var rand_x = rand_range(-1, 1)
	var rand_z = rand_range(-1,1)
	return Vector3(rand_x, 0, rand_z).normalized()
	
func bullet_hit(damage):
	sound_player.play_sound("Flesh_Hurt")
	health -= damage
	health = clamp(health, 0, max_health)
