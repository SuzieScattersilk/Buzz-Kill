extends State

#TODO: Fix this
func enter(_msg := {}) -> void:
	var new_dir = thinker.wander()
	thinker.direction = new_dir
	
func physics_process(delta: float) -> void:
	if thinker.health <= 0:
		thinker.die()
	var collision = thinker.move_and_collide(thinker.direction * thinker.move_speed * delta)
	if collision:
		var new_dir = thinker.wander()
		thinker.direction = new_dir
