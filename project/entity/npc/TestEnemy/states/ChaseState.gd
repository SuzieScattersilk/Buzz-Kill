extends State

var target: KinematicBody = null
var timer: float = 0
var target_last_seen: Vector3 = Vector3.INF
var target_visible: bool = false

func enter(_msg={"target": null}):
	print("Chase entered")
	thinker.current_speed = thinker.chase_speed
	var sounds = self.thinker.sounds
	sounds.play_sound(sounds.alert)
	timer = 0
	target = _msg["target"]
	if target:
		target_visible = true
		target_last_seen = target.position

# Called when the node enters the scene tree for the first time.
func process(delta):
	var coll = thinker.last_collision
	if coll != null:
		thinker.move_in_direction(self.thinker.current_direction.bounce(coll.normal))
	else:
		if thinker.can_see(target):
			target_visible = true
			target_last_seen = target.position
		elif thinker.position == target_last_seen:
			state_machine.transition_to("Alert", {"target": target})
		var toward_position = thinker.position.direction_to(target_last_seen)
		thinker.move_in_direction(toward_position, true)

func _on_FieldOfView_body_exited(body):
	if body == target:
		target_visible = false
