# TODO: convert to 3d; fix this
extends State

export var give_up_time = 5
export var direction_change_chance = 0.02

var timer: float = 0
var target: KinematicBody = null

func enter(_msg={"target": null}):
	print("Alert entered")
	thinker.current_speed = thinker.chase_speed
	timer = 0
	target = _msg["target"]

# Called when the node enters the scene tree for the first time.
func process(delta):
	if timer >= give_up_time:
		timer = 0
		self.state_machine.transition_to("Idle")
		return
	timer += delta
	var coll = thinker.last_collision
	if coll != null:
		thinker.move_in_direction(thinker.current_direction.bounce(coll.normal))
	else:
		if rand_range(0, 100) < direction_change_chance:
			thinker.move_in_direction(thinker._set_current_direction())

func _on_FieldOfView_body_entered(body):
	var target = body
	if target in get_tree().get_nodes_in_group("player"):
		if thinker.can_see(target):
			thinker.look_at_entity(target)
			state_machine.transition_to("Chase", {"target": target})
