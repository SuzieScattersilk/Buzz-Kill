class_name TestEnemy
extends KinematicBody

export var move_speed = 50
export var max_health = 4

var player = null
var dead = false
var direction
var health
var sound_player
var think_timer
var eyes

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("bots")
	sound_player = $Sound_Player
	eyes = $Eyes
	direction = wander()
	health = max_health
	
func _physics_process(delta):
	if dead:
		return
	if health <= 0:
		kill()
	else:
		var collision = move_and_collide(direction * move_speed * delta)
		if collision:
			print(collision)
			direction = wander()

func wander():
	var rand_x = rand_range(-1, 1)
	var rand_z = rand_range(-1,1)
	return Vector3(rand_x, 0, rand_z).normalized()
	
func kill():
	dead = true
	sound_player.play_sound("Flesh_Burst")
	$CollisionShape.disabled = true
	queue_free()
	
func bullet_hit(damage):
	sound_player.play_sound("Flesh_Hurt")
	health -= damage
	health = clamp(health, 0, max_health)
