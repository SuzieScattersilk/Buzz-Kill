# more-or-less the FPS Tutorial but refactored:
# https://docs.godotengine.org/en/stable/tutorials/3d/fps_tutorial/part_one.html#getting-everything-ready
# also uses code from ic3bug's '"Godot Strafe Jumping":
# https://github.com/ic3bug/Godot-Strafe-Jumping/blob/main/Scenes/Player.gd

extends KinematicBody
class_name Player

# PHYSICS STUFF
class MovementSettings:
	var max_speed : float = 0.0
	var acceleration : float = 0.0
	var deceleration : float = 0.0
	
	func _init(_max_speed, _acceleration, _deceleration):
		max_speed = _max_speed
		acceleration = _acceleration
		deceleration = _deceleration

var GROUND_SETTINGS : MovementSettings = MovementSettings.new(100.0, 14.0, 10.0)
var AIR_SETTINGS : MovementSettings = MovementSettings.new(70.0, 2.0, 2.0)
var STRAFE_SETTINGS : MovementSettings = MovementSettings.new(10.0, 50.0, 50.0)

# EXPORTS
export var MAX_HEALTH = 8
export var MAX_OVERHEAL = 12
export var SPEED_FACTOR = 1.0
export var RESPAWN_TIME = 4
export var enable_camera_lean = false
export var camera_lean_angle = 10
export var camera_lean_speed = 10
export var enable_stepping: bool = false
export var max_step_height = 0.35
export var min_step_height = 0.01
export var floor_snap_length = 0.1
export var AUTO_JUMP : bool = false
export var MAX_JUMPS = 2 #can be changed based on player class...?
export var JUMP_SPEED = 175
export var AIR_CONTROL : float = 1.5
export var MAX_SLOPE_ANGLE = 45
export var STOP_ON_SLOPE = false
export var MAX_SLIDES = 4
export var FRICTION : float = 12.0
export var GRAVITY = 500

#handlers, camera, extrenal stuff
onready var joypad_handler = $Rotation_Helper/JoypadHandler
onready var mouse_handler = $Rotation_Helper/MouseHandler
onready var camera = $Rotation_Helper/Camera
onready var rotation_helper = $Rotation_Helper
onready var animation_manager = $Rotation_Helper/Model/Animation_Player
onready var animation_tree = $Rotation_Helper/Model/AnimationTree
onready var animation_node = animation_tree.get("parameters/playback")
onready var globals = get_node("/root/Globals")
onready var inventory = $Rotation_Helper/PlayerInventory
onready var UI_status_label = $HUD/Panel/Gun_label
onready var sound_player = $Rotation_Helper/Sound_Player
onready var player_info = $Rotation_Helper/PlayerInfo
onready var floor_point = $FloorPoint
onready var floor_check = $FloorCheck

var player_class
#var MAX_HEALTH
#var MAX_OVERHEAL
var health

var dead_time = 0
var is_dead = false
var num_jumps = 0
var jump_queued : bool = false
var double_jump_queued: bool = false
var is_jump_held: bool = false # is the player holding down the jump button still?
var is_on_slope : bool = false
var dir = Vector3()
var dir_norm = Vector3.ZERO
var vel = Vector3()
var friction = 0.0

func _ready():
	health = MAX_HEALTH
	GROUND_SETTINGS.max_speed *= SPEED_FACTOR
	AIR_SETTINGS.max_speed *= SPEED_FACTOR
	STRAFE_SETTINGS.max_speed *= SPEED_FACTOR
	add_to_group("players")
	animation_manager.callback_function = funcref(self, "fire_bullet")
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	global_transform.origin = globals.get_respawn_position()
	inventory.player_node = self
	
func _physics_process(delta):
	if !is_dead:
		process_debug_hurt()
		process_input(delta)
		if joypad_handler.connected():
			process_joystick_look(delta)
		process_movement(delta)
		process_changing_weapons(delta)
	process_UI(delta)
	process_respawn(delta)

func get_input_movement_vector():
	var input_movement_vector = Vector2()
	input_movement_vector = Input.get_vector("movement_left", "movement_right", "movement_backward", "movement_forward")
	input_movement_vector = input_movement_vector.normalized()
	return input_movement_vector

func process_strafe(delta):
	dir = Vector3()
	# Get the camera's global transform so we can use its directional vectors
	var cam_xform = camera.get_global_transform()
	var input_movement_vector = get_input_movement_vector()
	dir += -cam_xform.basis.z.normalized() * input_movement_vector.y
	dir += cam_xform.basis.x.normalized() * input_movement_vector.x
	if enable_camera_lean:
		lean_camera(delta, input_movement_vector.x)

func process_select_weapon():
	inventory.process_select_weapon()
	
func process_fire():
	if Input.is_action_pressed("fire"):
		if inventory.changing_weapon == false:
			var current_weapon = inventory.get_current_weapon()
			if current_weapon != null:
				if current_weapon.ammo_in_weapon > 0:
					animation_node.travel(current_weapon.FIRE_NAME)

func process_debug_hurt():
	if Input.is_action_pressed("killbind"):
		add_overheal(-health)
	if Input.is_action_pressed("hurtbind"):
		add_overheal(-1)
		
func process_input(delta):
	process_strafe(delta)
	process_select_weapon()
	process_fire()
	if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func process_joystick_look(_delta):
	if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
		return
	camera_adjust(joypad_handler.get_right_stick(), joypad_handler.JOYPAD_SENSITIVITY)

func lean_camera(delta: float, axis: float):
	camera.rotation.z = lerp_angle(camera.rotation.z, deg2rad(axis * camera_lean_angle), delta * camera_lean_speed)
# credits to ic3bug for the movement code here:
# https://github.com/ic3bug/Godot-Strafe-Jumping/blob/main/Scenes/Player.gd
func process_movement(delta):
	queue_jump()
	if is_on_floor():
		num_jumps = 0
		ground_move(delta)
		if enable_stepping:
			process_stepping(delta)
	else:
		air_move(delta)
	# Move
	vel = move_and_slide(vel,Vector3.UP, STOP_ON_SLOPE, MAX_SLIDES, deg2rad(MAX_SLOPE_ANGLE))

func process_stepping(delta):
	var collision = move_and_collide(vel*delta, true, true, true) #i can't believe you can't use named params.
	if collision and not dir.is_equal_approx(Vector3.ZERO) and is_on_floor():
		floor_check.global_transform.origin = collision.position
		floor_check.transform.origin.y = 10 #return to floor position (note: this is whatever the current y position for the raycast is)
		if floor_check.is_colliding():
			var step_vector = floor_check.get_collision_point() - floor_point.global_transform.origin
			var step_height = step_vector.y
			if step_height < max_step_height && step_height > min_step_height:
				translate(step_vector * Vector3.UP)
				#Stop vertical movement to prevent player from unstepping
				vel.y = 0

func queue_jump():
	if AUTO_JUMP:
		jump_queued = Input.is_action_pressed("movement_jump")
		return
	jump_queued = false #always assume it's off; otherwise we get a weird flip-flop bug that makes us bounce twice
	if Input.is_action_pressed("movement_jump"):
		if is_jump_held: #should only fire once - no holding down unles AUTO_JUMP is on.
			return
		is_jump_held = true #hasn't fired yet!
		if !jump_queued: #a jump has NOT been queued yet...
			jump_queued = true #so queue one!
			return
#		print("Jump pressed; jump is not queued")
#		jump_queued = false #...does this code path ever get used? not touching it rn.
#		return
	else:
		is_jump_held = false

func air_move(delta):
	var accel = 0.0
	var wish_dir = Vector3(dir.x, 0, dir.z)
	var wish_speed = wish_dir.length()
	wish_speed *= AIR_SETTINGS.max_speed
#	var wish_dir_norm = wish_dir.normalized()
	var wish_speed_2 = wish_speed
	if vel.dot(wish_dir) < 0.0:
		accel = AIR_SETTINGS.deceleration
	else:
		accel = AIR_SETTINGS.acceleration
	if dir.z == 0.0 and dir.x != 0.0:
		if wish_speed > STRAFE_SETTINGS.max_speed:
			wish_speed = STRAFE_SETTINGS.max_speed
		accel = STRAFE_SETTINGS.acceleration
	accelerate(wish_dir, wish_speed, accel, delta)
	if AIR_CONTROL > 0:
		air_control(wish_dir, wish_speed_2, delta)
	if jump_queued and num_jumps < MAX_JUMPS: #the Nth jump where N > 1.
		num_jumps += 1
		vel.y = JUMP_SPEED
		# reset the velocity for a little more oomph and control
		vel.x = dir.x * wish_speed
		vel.z = dir.z * wish_speed
		jump_queued = false
	vel.y -= GRAVITY * delta
	
func air_control(target_dir, target_speed, delta):
	if abs(dir.z) < 0.001 or abs(target_speed) < 0.001:
		return
	var z_speed = vel.y
	vel.y = 0.0
	var speed = vel.length()
	vel = vel.normalized()
	var dot= vel.dot(target_dir)
	var k = 32.0
	k *= AIR_CONTROL * dot * dot * delta
	# Change direction while slowing down
	if dot > 0:
		vel.x *= speed + target_dir.x * k
		vel.y *= speed + target_dir.y * k
		vel.z *= speed + target_dir.z * k
		vel = vel.normalized()
		dir_norm = vel
	vel.x *= speed
	vel.y = z_speed
	vel.z *= speed
	
func ground_move(delta : float) -> void:
	apply_friction(float(!jump_queued), delta)
	var wishdir = Vector3(dir.x, 0.0, dir.z)
	wishdir = wishdir.normalized()
	dir_norm = wishdir #might wanna put a call to something like step_up() (for stairs) here
	var wishspeed : float = wishdir.length()
	wishspeed *= GROUND_SETTINGS.max_speed
	accelerate(wishdir, wishspeed, GROUND_SETTINGS.acceleration, delta)
	# Slope handling
	if get_slide_count() > 0:
		if get_slide_collision(0).normal.dot(Vector3.DOWN) > -0.9:
			is_on_slope = true
		else:
			# Apply gravity
			is_on_slope = false
			vel.y = -GRAVITY * delta
	if jump_queued and num_jumps < MAX_JUMPS: #the first jump.
		num_jumps += 1
		vel.y = JUMP_SPEED
		jump_queued = false

func apply_friction(t, delta):
	var vec = vel
	vec.y = 0.0
	var speed = vec.length()
	var drop = 0.0
	# Only apply friction when grounded
	if is_on_floor():
		var control
		if speed < GROUND_SETTINGS.deceleration:
			control = GROUND_SETTINGS.deceleration
		else:
			control = speed
		drop = control * FRICTION * delta * t
	var new_speed = speed - drop
	friction = new_speed
	if new_speed < 0:
		new_speed = 0
	if new_speed > 0:
		new_speed /= speed
	vel.x *= new_speed
	vel.z *= new_speed
	
func accelerate(target_dir, target_speed, accel, delta):
	var current_speed = vel.dot(target_dir)
	var add_speed = target_speed - current_speed
	if add_speed <= 0:
		return
	var accel_speed = accel * delta * target_speed
	if accel_speed > add_speed:
		accel_speed = add_speed
	vel.x += accel_speed * target_dir.x
	vel.z += accel_speed * target_dir.z

func process_changing_weapons(delta):
	inventory.process_changing_weapons(delta)

func process_UI(_delta):
	if !is_dead:
		if inventory.is_unarmed():
			UI_status_label.text = "HEALTH: " + str(health)
		else:
			var current_weapon = inventory.get_current_weapon()
			UI_status_label.text = "HEALTH: " + str(health) + \
			"\nAMMO:" + str(current_weapon.ammo_in_weapon) + "/" + \
			str(current_weapon.max_ammo)

func set_collision(value: bool):
	$Body_CollisionShape.disabled = !value
	$Feet_CollisionShape.disabled = !value
	
func show_death_screen(value: bool):
	$HUD/Death_Screen.visible = value
	$HUD/Panel.visible = !value
	$HUD/Crosshair.visible = !value

func die():
	set_collision(false)
	animation_node.travel("Idle_unarmed")
	dead_time = RESPAWN_TIME
	is_dead = true
	show_death_screen(true)
	
func respawn():
	inventory.dump_inventory()
	global_transform.origin = globals.get_respawn_position()
	set_collision(true)
	show_death_screen(false)
	health = MAX_HEALTH
	is_dead = false

func process_respawn(delta):
	if health <= 0 and !is_dead:
		die()
	if is_dead:
		dead_time -= delta
		var dead_time_pretty = str(dead_time).left(1)
		$HUD/Death_Screen/Label.text = "SQUASHED!\nRespawning in " + dead_time_pretty + "..."
		if dead_time <= 1:
			$HUD/Death_Screen/Label.text = "Prepare for battle..."
		if dead_time <= 0:
			respawn()

func scroll_input(event):
	inventory.scroll_input(event)

func select_weapon():
	if !inventory.changing_weapon:
		inventory.select_weapon()

func mouselook(event):
	if mouse_handler.mouse_moving(event):
		camera_adjust(event.relative, mouse_handler.MOUSE_SENSITIVITY)

func _input(event):
	if is_dead:
		return
	scroll_input(event)
	if !inventory.changing_weapon:
		select_weapon()
	mouselook(event)

func camera_adjust(source_vector2, sensitivity_constant):
	rotation_helper.rotate_x(deg2rad(source_vector2.y * sensitivity_constant))
	rotate_y(deg2rad(source_vector2.x * sensitivity_constant * -1))
	var camera_rot = rotation_helper.rotation_degrees
	camera_rot.x = clamp(camera_rot.x, -70, 70)
	rotation_helper.rotation_degrees = camera_rot

func fire_bullet():
	if !inventory.changing_weapon:
		inventory.get_current_weapon().fire_weapon()

func add_health(additional_health):
	health += additional_health
	health = clamp(health, 0, MAX_HEALTH)

func add_overheal(additional_health):
	health += additional_health
	health = clamp(health, 0, MAX_OVERHEAL)

func add_ammo(weapon_name, additional_ammo):
	#just plop the darn ammo in there (mom says i can't curse)
	if inventory.has_weapon(weapon_name):
		if inventory.weapons[weapon_name].consumes_ammo:
			inventory.weapons[weapon_name].add_ammo(additional_ammo)

func bullet_hit(damage):
	sound_player.play_sound("Flesh_Hurt")
	add_overheal(-damage) #doesn't erase any overheal + clamps to 0.

