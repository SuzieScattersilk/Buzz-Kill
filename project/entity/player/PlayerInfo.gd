# i have no idea if (mostly) data-only classes should be extending Node.
# feel free to refactor this somewhere else if you think it would
# be better for performance.

extends Node

class WeightClass:
	var MAX_HEALTH
	var MAX_OVERHEAL
	var SPEED_FACTOR

	func _init(max_health, max_overheal, speed_factor):
		MAX_HEALTH = max_health
		MAX_OVERHEAL = max_overheal
		SPEED_FACTOR = speed_factor

var MIDDLE = WeightClass.new(8, 12, 1);
var HEAVY = WeightClass.new(16, 24, 0.75);
var LIGHT = WeightClass.new(4, 8, 1.35);

class PlayerCharacter:
		
	var WEIGHT_CLASS # health, overheal, speed
	var FAVE_WEAPON_1 # for bots
	var FAVE_WEAPON_2 # for bots
	var PLAYER_NODE # for the ability.

	func _init(weight_class):
		WEIGHT_CLASS = weight_class

	func ability():
		# should be written per child class -
		# this is where the player node is important
		pass

# only for testing purposes - remove when the player 
# can actually select a char
var _TEST

func _init():
	_TEST = PlayerCharacter.new(MIDDLE)
