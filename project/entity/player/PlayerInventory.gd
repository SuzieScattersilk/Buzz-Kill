# MOST of this was in the player class,
# but has been moved here for the sake of cleanup and modularity.
# plus you can add weapons on the fly to it now! :D

extends Node

# Declare member variables here. Examples:
var current_weapon_name = "UNARMED"
var weapons = {"UNARMED":null,}
const WEAPON_NUMBER_TO_NAME = {0:"UNARMED"}
const WEAPON_NAME_TO_NUMBER = {"UNARMED":0}
var changing_weapon = false
var changing_weapon_name = "UNARMED"
var mouse_handler
var player_node
var selection_locked = false # for Super-weapons

# Called when the node enters the scene tree for the first time.
func _ready():
	current_weapon_name = "UNARMED"
	changing_weapon_name = "UNARMED"
	mouse_handler = get_node("../MouseHandler")
	
func reset_inventory():
	current_weapon_name = "UNARMED"
	changing_weapon_name = "UNARMED"
	changing_weapon = false
	selection_locked = false
	
func put_weapon(weapon):
#	print("Before: ", str(weapons))
	if weapon == null:
		return
	weapons[weapon.weapon_name] = weapon
	WEAPON_NAME_TO_NUMBER[weapon.weapon_name] = weapon.weapon_slot
	WEAPON_NUMBER_TO_NAME[weapon.weapon_slot] = weapon.weapon_name
	setup_weapon(weapon)
#	print("After: ", str(weapons))
#	print(weapon)
	
func get_current_weapon():
	return weapons[current_weapon_name]
	
func get(weapon_name):
	if !has_weapon(weapon_name):
		return null
	return weapons[weapon_name]
	
func dump_inventory():
	for weapon_name in weapons:
		var w = weapons[weapon_name]
		if w != null:
			WEAPON_NUMBER_TO_NAME.erase(w.weapon_slot)
			WEAPON_NAME_TO_NUMBER.erase(w.weapon_name)
			w.reset_weapon()
			weapons.erase(w.weapon_name)
	reset_inventory()
	
func has_weapon(weapon_name):
	#["Milhouse is not a meme voice"] UNARMED IS NOT A WEAPON!
	return weapon_name in weapons and weapons[weapon_name] != null
	
func is_unarmed():
	return current_weapon_name == "UNARMED"
	
func setup_weapon(weapon):
	var gun_aim_point_pos = get_node("../Gun_Aim_Point").global_transform.origin
	weapon.player_node = player_node
	weapon.look_at(gun_aim_point_pos, Vector3(0, 1, 0))
	weapon.rotate_object_local(Vector3(0, 1, 0), deg2rad(180))

func select_by_key(weapon_change_number):
	if Input.is_key_pressed(KEY_1):
		weapon_change_number = 0
	if Input.is_key_pressed(KEY_2):
		weapon_change_number = 1
	return weapon_change_number

func select_by_scroll(weapon_change_number):
	if Input.is_action_just_pressed("shift_weapon_positive"):
		weapon_change_number += 1
	if Input.is_action_just_pressed("shift_weapon_negative"):
		weapon_change_number -= 1
	weapon_change_number = clamp(weapon_change_number, 0, WEAPON_NUMBER_TO_NAME.size()-1)
	return weapon_change_number
	
func process_select_weapon():
	if selection_locked:
		return
	var weapon_change_number = WEAPON_NAME_TO_NUMBER[current_weapon_name]
	weapon_change_number = select_by_key(weapon_change_number)
	weapon_change_number = select_by_scroll(weapon_change_number)
	if changing_weapon == false:
		if WEAPON_NUMBER_TO_NAME[weapon_change_number] != current_weapon_name:
			changing_weapon_name = WEAPON_NUMBER_TO_NAME[weapon_change_number]
			changing_weapon = true
			mouse_handler.mouse_scroll_value = weapon_change_number

func process_changing_weapons(_delta):
	if selection_locked:
		return
	if changing_weapon == true:
#		print("process_changing_weapons(): current_weapon = ", current_weapon_name)
#		print("process_changing_weapons(): changing weapon? " , changing_weapon)
		var weapon_unequipped = false
		var current_weapon = weapons[current_weapon_name]
		if current_weapon == null:
#			print("Current weapon is null; auto-unequipped.")
			weapon_unequipped = true
		else:
			if current_weapon.is_weapon_enabled == true:
				weapon_unequipped = current_weapon.unequip_weapon()
#				print("Disabling current weapon.")
			else:
				weapon_unequipped = true
#				print("Current weapon is unequipped.")
		if weapon_unequipped == true:
			var weapon_equiped = false
			var weapon_to_equip = weapons[changing_weapon_name]
#			print("process_changing_weapons(): weapon_to_equip = ", changing_weapon_name)
			if weapon_to_equip == null:
#				print("Next weapon is null; auto-equipped next weapon.")
				weapon_equiped = true
			else:
				if weapon_to_equip.is_weapon_enabled == false:
					weapon_equiped = weapon_to_equip.equip_weapon()
#					print("Equipping ", weapon_to_equip, ".")
				else:
#					print(weapon_to_equip, " has been equipped.")
					weapon_equiped = true
#			print("process_changing_weapons(): new weapon equipped? ", weapon_equiped)
			if weapon_equiped == true:
				changing_weapon = false
				current_weapon_name = changing_weapon_name
				changing_weapon_name = ""
#				print("process_changing_weapons(): current_weapon = ", current_weapon_name)
#				print()

func scroll_input(event):
	mouse_handler.clamped_scroll_input(event, 0, WEAPON_NUMBER_TO_NAME.size()-1)

func select_weapon():
	if selection_locked:
		return
	var index = int(mouse_handler.get_mouse_scroll_value())
	if WEAPON_NUMBER_TO_NAME[index] != current_weapon_name:
		_select_by_index(index)

func _select_by_index(index: int):
#			print("select_weapon(): index = " + str(index))
#			print("select_weapon(): weapons are different? " + str(WEAPON_NUMBER_TO_NAME[index] != current_weapon_name))
			changing_weapon_name = WEAPON_NUMBER_TO_NAME[index]
			changing_weapon = true
			mouse_handler.set_mouse_scroll_value(index)

func force_select_weapon(index: int):
	var ret = 0
	ret = select_by_key(index)
	ret = select_by_scroll(index)
	ret = clamp(ret, 0, WEAPON_NUMBER_TO_NAME.size()-1)
	return ret
