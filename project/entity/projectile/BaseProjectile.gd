extends RigidBody
class_name BaseProjectile

export var damage = 1
export var kill_timer = 1 # seconds before the weapon disappears. 0 = no kill timer.
export var hurt_shooter = false #most projectiles should not hurt the player who shot them, but this can be changed.
export var bounces = false
export var bounces_off_players = false
export var has_timed_effect = false
export var is_explosive = true
export var effect_timer = 0
export var is_homing_missile = false
export var steering_force = 50.0
export var muzzle_velocity = 500

var shooter_node # make sure to assign this in the weapon's spawn_projectile() method, not in here.
var target = null # assigned when the projectile hits something.
var homing_target = null #assigned to homing projectiles
var globals
var timer = 0
var time_of_impact = 0
var collided: bool = false
var sound_player
var effect_sound_played = false

export var bounce_sound = ""
export var effect_sound = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("body_entered", self, "collided")
	globals = get_tree().get_root().get_node("Globals") # projectiles need their own sound player
	sound_player = $Sound_Player
	effect_sound_played = false
#	print("Sound player: ", sound_player)

func play_bounce_sound():
	if bounce_sound:
		sound_player.play_sound(bounce_sound)
		
func play_effect_sound():
	if effect_sound and !effect_sound_played:
		sound_player.play_sound(effect_sound)
		effect_sound_played = true

func _process(delta):
	if kill_timer > 0:
		if is_homing_missile:
			find_target()
			apply_central_impulse(seek())
		process_kill_timer(delta)

func process_kill_timer(delta):
	timer += delta
	if timer >= kill_timer:
		if has_timed_effect: #i.e. like a grenade
			if is_explosive:
				play_effect_sound()
			apply_effect()
		if timer >= kill_timer + effect_timer:
			queue_free()

func collided(body):
	if !bounces:
		time_of_impact = timer
		target = body
		if target == null:
			if is_explosive:
				play_effect_sound()
			apply_effect()
			if timer >= time_of_impact + effect_timer:
				queue_free()
			return
		if !bounces_off_players:
			if is_explosive:
				play_effect_sound()
			apply_effect()
			if timer >= time_of_impact + effect_timer:
				queue_free()
		return
	else:
		play_bounce_sound()

func find_target():
	var potential_targets = get_tree().get_nodes_in_group("npc")
	var chosen_target = null
	var min_distance = INF
	
	for potential_target in potential_targets:
		var dist = global_transform.origin.distance_to(potential_target.global_transform.origin)
		if dist < min_distance:
			min_distance = dist
			chosen_target = potential_target
	
	return chosen_target
	
		
func seek():
	var steer = Vector3.ZERO
	if homing_target != null:
		var wishdir = (homing_target.transform.origin - transform.origin).normalized() * muzzle_velocity
		steer = (wishdir - linear_velocity).normalized() * steering_force
	return steer

# "Apply effect" is different for every projectile, I'd assume, so...
# this should be overriden per class	
#todo: how to decouple this from needing to hit something (i.e. timed rockets; remote projectiles)
func apply_effect():
	if !is_explosive:
		play_effect_sound()
	if target.has_method("bullet_hit"):
		if hurt_shooter:
#			print("Applying effect to target ", target)
			target.bullet_hit(damage) #this can hurt the player if true
		elif shooter_node != target:
#			print("Applying effect to target ", target)
			target.bullet_hit(damage) #otherwise, make sure this is NOT the player.
		else: #if it's not the player and it's not anyone else, then who could it be?
			return
