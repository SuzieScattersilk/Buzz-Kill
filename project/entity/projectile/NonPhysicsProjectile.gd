class_name NonPhysicsProjectile
extends Spatial

export var damage = 1
export var muzzle_velocity = 500
export var kill_timer = 1.0 # seconds before the weapon disappears. 0 = no kill timer.
export var hurt_shooter = false #most projectiles should not hurt the player who shot them, but this can be changed.
export var has_timed_effect = false
export var effect_timer = 0.0
export var is_explosive = true
export var is_homing_missile = false
export var steering_force = 50.0
export var homing_start_delay = 3.0
export var effect_sound = ""

var shooter_node # make sure to assign this in the weapon's spawn_projectile() method, not in here.
var target = null # assigned when the projectile hits something.
var homing_target = null #assigned to homing projectiles
var globals
var timer = 0
var time_of_impact = 0
var collided: bool = false
var sound_player
var effect_sound_played = false
var forward_dir
var accel

func _ready():
	# We want to get the area and connect ourself to it's body_entered signal.
	# This is so we can tell when we've collided with an object.
	$Area.connect("body_entered", self, "collided")
	globals = get_tree().get_root().get_node("Globals") # projectiles need their own sound player
	sound_player = $Sound_Player
	effect_sound_played = false
	forward_dir = global_transform.basis.z.normalized() * muzzle_velocity
	accel = Vector3.ZERO
	

func _physics_process(delta):
	process_motion(delta)
	process_kill_timer(delta)
	
func play_effect_sound():
	if effect_sound and !effect_sound_played:
		sound_player.play_sound(effect_sound)
		effect_sound_played = true
	
func process_kill_timer(delta):
	timer += delta
	if timer >= kill_timer:
		if has_timed_effect: #i.e. like a grenade
			if is_explosive:
				play_effect_sound()
			apply_effect()
		if timer >= kill_timer + effect_timer:
			queue_free()

func process_motion(delta):
	if is_homing_missile and timer >= homing_start_delay:
		homing_target = find_target()
		accel += seek()
		forward_dir += accel * delta
		forward_dir = forward_dir.normalized()
	else:
		forward_dir = global_transform.basis.z.normalized() * muzzle_velocity * delta
	global_translate(forward_dir)
		
		
func find_target():
	var potential_targets = get_tree().get_nodes_in_group("npc")
	var chosen_target = null
	var min_distance = INF
	
	for potential_target in potential_targets:
		var dist = global_transform.origin.distance_to(potential_target.global_transform.origin)
		if dist < min_distance:
			min_distance = dist
			chosen_target = potential_target
	
	return chosen_target
	
		
func seek():
	var steer = Vector3.ZERO
	if homing_target != null:
		var wishdir = (homing_target.transform.origin - transform.origin).normalized() * muzzle_velocity
		steer = (wishdir - forward_dir).normalized() * steering_force
	return steer

func collided(body):
	time_of_impact = timer
	target = body
	if is_explosive:
		play_effect_sound()
	if target != null:
		apply_effect()
	play_effect_sound()
	if timer >= time_of_impact + effect_timer:
		queue_free()
	
func apply_effect():
	if !is_explosive:
		play_effect_sound()
	if target.has_method("bullet_hit"):
		if hurt_shooter:
			target.bullet_hit(damage) #this can hurt the player if true
		elif shooter_node != target:
			target.bullet_hit(damage) #otherwise, make sure this is NOT the player.
		else: #if it's not the player and it's not anyone else, then who could it be?
			return
