class_name PhysicsProjectile
extends RigidBody

var collision_shape
var mesh
var blast_area
var explosion_particles
		
export var damage = 1
export var muzzle_velocity = 500
export var kill_timer = 1 # seconds before the weapon disappears. 0 = no kill timer.
export var hurt_shooter = false #most projectiles should not hurt the player who shot them, but this can be changed.
export var bounces = false
export var bounces_off_players = false
export var has_timed_effect = false
export var effect_timer = 0
export var is_explosive = true
export var bounce_sound = ""
export var effect_sound = ""

var shooter_node # make sure to assign this in the weapon's spawn_projectile() method, not in here.
var target = null # assigned when the projectile hits something.
var globals
var timer = 0
var time_of_impact = 0
var collided: bool = false
var sound_player
var effect_sound_played = false

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("body_entered", self, "collided")
	globals = get_tree().get_root().get_node("Globals") # projectiles need their own sound player
	sound_player = $Sound_Player
	effect_sound_played = false
	collision_shape = $Collision_Shape
	mesh = $MeshInstance
	blast_area = $Blast_Area
	explosion_particles = $Explosion
	
	# Make sure the explosion particles are not emitting, and make sure one_shot is enabled.
	explosion_particles.emitting = false
	explosion_particles.one_shot = true
#	print("Sound player: ", sound_player)
#	print("Sound player: ", sound_player)

func play_bounce_sound():
	if bounce_sound:
		sound_player.play_sound(bounce_sound)
		
func play_effect_sound():
	if effect_sound and !effect_sound_played:
		sound_player.play_sound(effect_sound)
		effect_sound_played = true

func _process(delta):
	if kill_timer > 0:
		process_kill_timer(delta)

func process_kill_timer(delta):
	timer += delta
	if timer >= kill_timer:
		if has_timed_effect: #i.e. like a grenade
			if is_explosive:
				play_effect_sound()
			apply_effect()
		if timer >= kill_timer + effect_timer:
			queue_free()

func collided(body):
	time_of_impact = timer
	target = body
	if bounces:
		if not target.has_method("bullet_hit"):
			play_bounce_sound()
		elif bounces_off_players:
			play_bounce_sound()
		else:
			handle_impact()
	else:
		handle_impact()
			
		
			
func handle_impact():
	if is_explosive:
		play_effect_sound()
	apply_effect()
	if timer >= time_of_impact + effect_timer:
		queue_free()


func apply_effect():
	explosion_particles.emitting = true
	mesh.visible = false
	collision_shape.disabled = true
	mode = RigidBody.MODE_STATIC
	if !is_explosive:
		play_effect_sound()
	var bodies = blast_area.get_overlapping_bodies()
	for body in bodies:
		target = body
		if target.has_method("bullet_hit"):
			if hurt_shooter:
				target.bullet_hit(damage) #this can hurt the player if true
			elif shooter_node != target:
				target.bullet_hit(damage) #otherwise, make sure this is NOT the player.
			else: #if it's not the player and it's not anyone else, then who could it be?
				return

